# Ansible

Ansible is used to privision Linux installations into ready-to-go environments. Setting up firewalls, authorised SSH keys, installing Kubernetes, are all tasks done via Ansible. This is done to keep Linux box installations as quick and reliable as possible.

## Getting started
To get started, first clone the repository into your local machine. Afterwards, install the external roles. This can be done with the following command;
```bash
make install
```

## Provisioning a Kubernetes cluster
To provision a cluster, as well as provision new nodes to join an existing cluster, use the below command. Be sure that the relevant `hosts.ini` file is updated whenever adding new machines to the cluster.
```bash
make cluster
```