default: cluster

install:
	ansible-galaxy install -r requirements.yml

cluster:
	ANSIBLE_STDOUT_CALLBACK=yaml ansible-playbook playbooks/process/setup_cluster.yml -i inventory/default/hosts.ini
